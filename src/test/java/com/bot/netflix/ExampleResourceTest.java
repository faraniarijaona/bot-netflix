package com.bot.netflix;

import com.bot.netflix.db.repository.AbonnementRepository;
import com.bot.netflix.db.repository.UserRepository;
import com.bot.netflix.db.model.Abonnement;
import com.bot.netflix.db.model.NetflixAccount;
import com.bot.netflix.db.model.User;
import com.bot.netflix.fbelement.elements.Message;
import com.bot.netflix.utilities.parsingUtilities.ExtractAmountUtil;
import com.bot.netflix.utilities.parsingUtilities.ExtractReferenceUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.quarkus.test.junit.QuarkusTest;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class ExampleResourceTest {

    @Inject
    UserRepository userRepository;

    @Inject
    AbonnementRepository abonnementRepository;

    @Test
    public void testHelloEndpoint() {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(new Date());

        calendar.add(Calendar.DATE, -7);

        System.out.println(calendar.getTime().toString());

        given()
          .when().get("/hello")
          .then()
             .statusCode(200)
             .body(is("hello"));
    }

    @Test
    public void testInsert(){
       User user = new User()
              /* .setUserId("blabla")
               .setFirstName("fname")
               .setLastName("last")
               .setGender("male")
               .setProfilPic("kdsfdsf")*/;


        Assertions.assertTrue(userRepository.findy("blabla").isPresent());
    }

    @Test
    public void testdelete(){
               userRepository.remove("blabla");
        Assertions.assertTrue(!userRepository.findy("blabla").isPresent());
    }

    @Test
    public void testAbonnement(){
        List<NetflixAccount> accounts = abonnementRepository.getListAvailableAccount();
        int duration = 3;

        User user = new User()/*
                .setProfilPic("https:///")
               // .setGender("male")
                .setLastName("lastName")
                .setFirstName("firstName")
                .setUserId("userId")
                .setDate(Calendar.getInstance().getTime());*/;

        NetflixAccount netflixAccount = accounts.get(0);
        String code = RandomString.make(6);
        long amount = duration * 20700;

        int day_of_month = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        if(day_of_month > 15){
            //Si la date d'aujourd'hui est superieur du 15
            duration = duration + 1;

            if(day_of_month < 26){
                amount = amount + 7000;
            }
        }

        Calendar date_end = Calendar.getInstance();
        date_end.add(Calendar.MONTH, duration);

        date_end.set(Calendar.DAY_OF_MONTH, 9);
        date_end.set(Calendar.HOUR,0);
        date_end.set(Calendar.MINUTE,0);
        date_end.set(Calendar.SECOND,0);

        Abonnement newAbonnement = new Abonnement();
        newAbonnement.setAmount(Double.valueOf(amount));
        newAbonnement.setCode(code);
        newAbonnement.setDateEnd(date_end.getTime());
        newAbonnement.setDuration(duration);



        Assert.assertTrue(accounts.size()  == 1);
    }

    @Test
    public void testRandomStr(){
        Calendar date_end = Calendar.getInstance();
        date_end.add(Calendar.MONTH, 2);

        date_end.set(Calendar.DAY_OF_MONTH, 9);
        date_end.set(Calendar.HOUR,0);
        date_end.set(Calendar.MINUTE,0);
        date_end.set(Calendar.SECOND,0);

        System.out.println(date_end.getTime().toString());

        String p = RandomString.make(6);
        System.out.println(p);
        System.out.println(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        Assert.assertTrue(p.length() == 6);
    }

    @Test
    public void testMessage(){
        Message message = new Message();

        List<String> strings = new ArrayList<>();
        strings.add("payment");

        JsonObject object = new JsonObject();
        object.addProperty("amount", 580000);
        object.addProperty("code", "dsfdsfdf");

        message.setmSetAttributes(object);
        message.setmBlockNames(strings);

        System.out.println(new Gson().toJson(message));
    }

    @Test
    public void testAbonnement2(){

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            Date d = dateFormat.parse(dateFormat.format(date));
            System.out.println(d.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(new Date().getTime());
    }

    @Test
    public  void testRegexp(){
        System.out.println( ExtractReferenceUtil.find(" Votre dépot de 20 700 Ar  par le 0325001021 est réussi. Nouveau solde: 288 551 Ar .Trans Id :CI200504.1316.B01599. Orange Money vous remercie.\n" +
                "\n" +
                "\n" +
                "\n" +
                "Vous avez recu un transfert de 20700Ar venant du 0320227138 Nouveau Solde: 267851Ar.  Trans Id: PP200503.2002.B80520. Orange Money vous remercie.\n" +
                "\n" +
                "\n" +
                "\n" +
                "Vous avez recu un transfert de 500Ar par carte VISA. Raison: test. Frais: 0Ar. Nouveau solde: 420034.0Ar. Trans id: CI200420.1510.A38157. Orangemoney vous remercie.\n" +
                "\n" +
                "\n" +
                "\n" +
                "1/3 Vous avez recu 20 700 Ar de la part de Kiadyandrimpitiavana (0345225457) le 04/05/20 a 19:00. Raison: Flix 3/ Kiady. Votre solde est de 339 898 Ar. Ref:\n" +
                "\n" +
                "\n" +
                "\n" +
                "2/3 1025874763\n" +
                "\n" +
                "\n" +
                "\n" +
                "3/3 Beneficiez d'un bonus de 20 pourcent en rechargeant votre compte TELMA via MVola en appelant au #111#. MVola Simple Immediat Securise.\n" +
                "\n" +
                "\n" +
                "\n" +
                "1/3 Vous avez credite votre compte de 64 100 Ar aupres de ANDRIAMIARAMBOHITRA§RAJONIRINA§MICKAEL 2 (0347808351) le 04/05/20 a 09:32. Votre solde est de 155 298\n" +
                "\n" +
                "\n" +
                "\n" +
                "2/3 Ar. Ref: 1019209178\n" +
                "\n" +
                "\n" +
                "\n" +
                "3/3 Beneficiez d'un bonus de 20 pourcent en rechargeant votre compte TELMA via MVola en appelant au #111#. MVola Simple Immediat Securise. ")
        );
    }

    @Test
    public void testParse(){
     /* Pair p =   ParseMessage.parseOrange("Votre dépot de 20 700 Ar  par le 0325001021 est réussi. Nouveau solde: 288 551 Ar .Trans Id :CI200504.1316.B01599. Orange Money vous remercie");
        System.out.println(p.getKey() + " -- "+p.getValue());
        p =   ParseMessage.parseOrange("Vous avez recu un transfert de 20700Ar venant du 0320227138 Nouveau Solde: 267851Ar.  Trans Id: PP200503.2002.B80520. Orange Money vous remercie");
        System.out.println(p.getKey() + " -- "+p.getValue());
      //  System.out.println(ExtractAmountUtil.forOrange(""));*/
        System.out.println(ExtractAmountUtil.forTelma("1/3 Vous avez recu 20 700 Ar de la part de Kiadyandrimpitiavana (0345225457) le 04/05/20 a 19:00. Raison: Flix 3/ Kiady. Votre solde est de 339 898 Ar. Ref:"));
    }

}