package com.bot.netflix;

import com.bot.netflix.db.repository.AbonnementRepository;
import com.bot.netflix.utilities.AbonnementUtilities;
import io.quarkus.runtime.StartupEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.Date;

@ApplicationScoped
public class Application {

    @Inject
    AbonnementRepository abonnementRepository;

    @Inject
    AbonnementUtilities abonnementUtilities;

    @Transactional
    void onStart(@Observes StartupEvent ev) {
        System.out.println("executing abonnement cleanup");
        abonnementRepository.list("is_paid = 0")
                .forEach(abonnement -> {
                    if ((abonnement.getDate().getTime() + 7000000) <= new Date().getTime()) {
                        abonnement.delete();
                    } else {
                        abonnementUtilities.deleteAbonnement(abonnement.getCode(), abonnement.getUser().getUserId(), abonnement.getDate().getTime() + 7200000 - Instant.now().getLong(ChronoField.MILLI_OF_SECOND));
                    }
                });
        System.out.println("Application is starting ...");
    }
}
