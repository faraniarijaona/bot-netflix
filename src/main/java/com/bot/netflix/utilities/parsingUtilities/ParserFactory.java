package com.bot.netflix.utilities.parsingUtilities;

public class ParserFactory {
    public static IParser getInstance(String sender) {
        if(EnumSender.MVOLA.name().equalsIgnoreCase(sender.trim())){
            return new ParseTelma();
        }

        if(EnumSender.ORANGEMONEY.name().equalsIgnoreCase(sender.trim()))
            return new ParseOrange();

        return  null;
    }
}