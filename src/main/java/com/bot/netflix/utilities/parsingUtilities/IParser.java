package com.bot.netflix.utilities.parsingUtilities;

import javafx.util.Pair;

public interface IParser {
    public Pair<String, Double> parse(String message);
}
