package com.bot.netflix.utilities.parsingUtilities;

public class ExtractAmountUtil {
    public static long forOrange(String message) {
        String pattern1 = "Vous avez recu un transfert de";
        String pattern2 = "Votre dépot de";
        String pattern3 = "Ar";

        message = message.replace(pattern1, "").replace(pattern2, "");

        message = message.substring(0, message.indexOf("Ar")).replace(" ", "").trim();

        return Double.valueOf(message).longValue();
    }

    public static long forTelma(String message) {
        String pattern1 = "1/3 Vous avez recu";
        String pattern2 = "1/3 Vous avez credite votre compte de";
        String pattern3 = "Ar";

        message = message.replace(pattern1, "").replace(pattern2, "");

        message = message.substring(0, message.indexOf("Ar")).replace(" ", "").trim();

        return Double.valueOf(message).longValue();
    }
}
