package com.bot.netflix.utilities.parsingUtilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ExtractReferenceUtil {
    static String telma_pattern = "\\d{9,}";

    public static List<String> find(String text) {
        if (text.split("\\s").length == 1) {
            List<String> s = new ArrayList<>();
            s.add(text);

            return s;
        }

        List<String> strings = Arrays.stream(text.split("\\s"))
                .filter(s -> s.length() > 8)
                .filter(s -> s.charAt(0) != '0')
                .filter(s -> Pattern.matches(telma_pattern, s)).collect(Collectors.toList());

        strings.addAll(Arrays.stream(text.split("\\s"))
                .filter(s -> s.length() > 16)
                .map(s -> s.charAt(0) == ':' ? s.substring(1) : s)
                .map(s -> s.charAt(s.length() - 1) == '.' ? s.substring(0, s.length() - 1) : s)
                .filter(s -> s.substring(0, 2).equals("PP") || s.substring(0, 2).equals("CI")).collect(Collectors.toList()));

        return strings;
    }

    public static List<String> findTelma(String text) {
        if (text.split("\\s").length == 1) {
            List<String> s = new ArrayList<>();
            s.add(text);

            return s;
        }

        List<String> strings = Arrays.stream(text.split("\\s"))
                .filter(s -> s.length() > 8)
                .filter(s -> s.charAt(0) != '0')
                .filter(s -> Pattern.matches(telma_pattern, s)).collect(Collectors.toList());

        return strings;
    }

    public static List<String> findOrange(String text) {
        if (text.split("\\s").length == 1) {
            List<String> s = new ArrayList<>();
            s.add(text);

            return s;
        }

        List<String> strings = Arrays.stream(text.split("\\s"))
                .filter(s -> s.length() > 16)
                .map(s -> s.charAt(0) == ':' ? s.substring(1) : s)
                .map(s -> s.charAt(s.length() - 1) == '.' ? s.substring(0, s.length() - 1) : s)
                .filter(s -> s.substring(0, 2).equals("PP") || s.substring(0, 2).equals("CI")).collect(Collectors.toList());

        return strings;
    }
}
