package com.bot.netflix.utilities.parsingUtilities;

import javafx.util.Pair;

import java.util.List;

public class ParseOrange implements IParser {
    @Override
    public Pair<String, Double> parse(String message) {

        String pattern1 = "Vous avez recu un transfert de";
        String pattern2 = "Votre dépot de";

        if (!message.contains(pattern1) && !message.contains(pattern2))
            return null;

        long forAmount = ExtractAmountUtil.forOrange(message);

        List<String> refs = ExtractReferenceUtil.findOrange(message);

        return refs.size() == 0 ? null : new Pair(refs.get(0), (double) forAmount);
    }
}
