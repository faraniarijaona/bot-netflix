package com.bot.netflix.utilities.parsingUtilities;

import javafx.util.Pair;

import java.util.List;

public class ParseTelma implements IParser {
    @Override
    public Pair<String, Double> parse(String message) {
        String pattern3 = "1/3 Vous avez recu";
        String pattern33 = "1/2 Vous avez recu";
        String pattern4 = "1/3 Vous avez credite votre compte de";
        String pattern44 = "1/2 Vous avez credite votre compte de";

        if (!message.contains(pattern3) && !message.contains(pattern4) && !message.contains(pattern33) && !message.contains(pattern44))
            return null;

        long forAmount = ExtractAmountUtil.forTelma(message);

        List<String> refs = ExtractReferenceUtil.findTelma(message);

        return refs.size() == 0 ? null : new Pair(refs.get(0), (double)forAmount);
    }
}
