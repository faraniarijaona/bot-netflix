package com.bot.netflix.utilities;

import com.bot.netflix.db.model.Abonnement;
import com.bot.netflix.db.repository.AbonnementRepository;
import io.vertx.core.Vertx;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.Date;
import java.util.function.Predicate;

@ApplicationScoped
public class AbonnementUtilities {
    @Inject
    private AbonnementRepository abonnementRepository;

    Comparator<Abonnement> abonnementComparator = Comparator.comparing(abonnement -> abonnement.getDateEnd().getTime());
    Predicate<Abonnement> abonnementPredicate = abonnement -> abonnement.getDateEnd().getTime()> new Date().getTime();

    public boolean abonnementPredicate(Abonnement abonnement) {
        return  abonnementPredicate.test(abonnement);
    }

    public int abonnementComparator(Abonnement abonnement, Abonnement abonnement1) {
        return abonnementComparator.compare(abonnement, abonnement1);
    }

    @Transactional
    public void deleteAbonnement(String code, String user_id){
        Vertx.vertx().setTimer(7200000, event -> {
            try {
                abonnementRepository.supprimer(code, user_id);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    @Transactional
    public void deleteAbonnement(String code, String user_id, long in_ms){
        Vertx.vertx().setTimer(in_ms, event -> {
            try {
                abonnementRepository.supprimer(code, user_id);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    /**
     *  trim unexpected char
     * @param ref the ref payment
     * @return cleaned ref payment
     */
    public static String cleanRefPayment(String ref){
        ref = ref.trim();
        int end_index = ref.length() - 1;

        if(ref.charAt(end_index) =='.'){
            return ref.substring(0, end_index).trim();
        }
        return ref;
    }
}
