package com.bot.netflix.utilities;

import com.bot.netflix.fbelement.elements.Message;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class MessageUtilities {
    static Gson gson = new Gson();

    public static String toJson(Object message) {
        return gson.toJson(message);
    }

    public static String toJson(Message message) {
        return gson.toJson(message);
    }

    public static String toJson(io.vertx.core.json.JsonObject message) {
        return gson.toJson(message);
    }

    public static String createRedirectMessage(String block) {
        Message message = new Message();
        List<String> blocks_name = new ArrayList<>();
        blocks_name.add(block);
        message.setmBlockNames(blocks_name);
        return MessageUtilities.toJson(message);
    }

    public static String createRedirectMessage(JsonObject attributes, String block) {
        Message message = new Message();
        message.setmSetAttributes(attributes);
        List<String> blocks_name = new ArrayList<>();
        blocks_name.add(block);
        message.setmBlockNames(blocks_name);
        return MessageUtilities.toJson(message);
    }
}
