package com.bot.netflix.utilities;

import io.vertx.core.logging.Log4j2LogDelegateFactory;
import oma.logging.LogConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Singleton;

@Singleton
public class LogUtil {

    /*
     * Configure logger to use log4j2.xml
     */
    public LogUtil() {
        System.setProperty("vertx.logger-delegate-factory-class-name", Log4j2LogDelegateFactory.class.getName());
        LogConfigurator.configure("log4j2.xml");
    }

    /**
     * Get logger by name
     * @param loggerName the logger name
     * @return logger
     */
    public Logger getLogger(String loggerName){
      return LogManager.getLogger(loggerName);
    }
}
