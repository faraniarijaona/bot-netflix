package com.bot.netflix.utilities.response;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Response {
    Status status;
    String ref;
    String message;

    public Response() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
