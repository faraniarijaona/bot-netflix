package com.bot.netflix.db.repository;

import com.bot.netflix.db.model.Abonnement;
import com.bot.netflix.db.model.NetflixAccount;
import com.bot.netflix.db.model.User;
import com.bot.netflix.utilities.LogUtil;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Parameters;
import io.vertx.core.json.JsonObject;
import oma.logging.LogRecord;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.context.ManagedExecutor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@ApplicationScoped
public class AbonnementRepository implements PanacheRepositoryBase<Abonnement, Integer> {

    @Inject
    LogUtil logUtil;

    private Logger LOGGER() {
        return logUtil.getLogger("dbLogger");
    }

    @Inject
    ManagedExecutor managedExecutor;

    @Inject
    UserRepository userRepository;

    /**
     * Obtenir la liste des comptes disponible pour vente
     *
     * @return la liste, empty list si pas de compte disponible
     */
    public List<NetflixAccount> getListAvailableAccount() {
        try {
            return CompletableFuture.supplyAsync(() -> {

                List<NetflixAccount> accounts = NetflixAccount.listAll();

                return accounts.stream().filter(netflixAccount -> netflixAccount
                        .getAbonnementCollection().stream().noneMatch(abonnement -> abonnement.getDateEnd().compareTo(new Date()) > 0))
                        .collect(Collectors.toList());
            }, managedExecutor).toCompletableFuture().get();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * creation nouveau Abonnement - creation Ou Update User
     *
     * @param abonnement nouveau Abonnement
     * @param isOldUser  verifie si l'User qui  demande de s'abonner est déjà client ou non
     * @return completableFuture
     */
    @Transactional
    public CompletableFuture enregistrer(Abonnement abonnement, boolean isOldUser) {

        abonnement.setDate(new Date());
        abonnement.setIsPaid(false);

        LogRecord LOG_RECORD = new LogRecord().setService("abonnement.enregistrer");
        JsonObject LOG_MESSAGE = new JsonObject().put("abonnement.userId", abonnement.getUser().getUserId())
                .put("abonnement.code", abonnement.getCode()).put("abonnement.ispaid", abonnement.getIsPaid());

        return CompletableFuture.runAsync(() -> {
            if (isOldUser) {
                User user = User.findById(abonnement.getUser().getUserId(), LockModeType.PESSIMISTIC_WRITE);
                user.setFirstName(abonnement.getUser().getFirstName());
                user.setLastName(abonnement.getUser().getLastName());
                user.setGender(abonnement.getUser().getGender());
                user.setProfilPic(abonnement.getUser().getProfilPic());

                user.flush();
            } else {
                abonnement.getUser().setDate(new Date());
                User.persist(abonnement.getUser());
                abonnement.getUser().flush();
            }

            Abonnement.persist(abonnement);
            abonnement.flush();

            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response", "enregistrer success")));

        }, managedExecutor).toCompletableFuture();
    }

    /**
     * Enregistre le code de paiement de l'abonnement pour confirmer ceux-ci
     *
     * @param abonnement l'abonnement
     */
    @Transactional
    public void valideAbonnement(Abonnement abonnement) {

        LogRecord LOG_RECORD = new LogRecord().setService("abonnement.valideAbonnement");
        JsonObject LOG_MESSAGE = new JsonObject().put("abonnement.userId", abonnement.getUser().getUserId())
                .put("abonnement.code", abonnement.getCode());

        Abonnement abonnement1 = Abonnement.findById(abonnement.getId());
        abonnement1.setIsPaid(true);
        abonnement1.setPaymentLog(abonnement.getPaymentLog());
        abonnement1.flush();

        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response", "enregistrer success")));
    }

    /**
     * Suppresseion d'un abonnement abandoné
     *
     * @param code    le code de l'abonnement
     * @param user_id l'user_id
     */
    @Transactional
    public void supprimer(String code, String user_id) {
        Abonnement.delete("code = :code and user_id = :user_id and is_paid = :is_paid",
                Parameters.with("code", code)
                        .and("user_id", user_id)
                        .and("is_paid", 0)
        );

        flush();
    }

    /**
     * Recherche un abonnement pas encore valider
     *
     * @param user_id l'user_id
     * @param code    le code de l'abonnement
     * @return Optional abonnement
     */
    public Optional<Abonnement> rechercher(String user_id, String code) {
        Optional<User> user = userRepository.findy(user_id);

        if (!user.isPresent()) {
            return Optional.empty();
        }

        return user.get().getAbonnementCollection().stream()
                .filter(abonnement -> abonnement.getCode().equals(code))
                .filter(abonnement -> !abonnement.getIsPaid()).findFirst();
    }
}
