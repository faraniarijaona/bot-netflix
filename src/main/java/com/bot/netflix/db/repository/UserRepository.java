package com.bot.netflix.db.repository;


import com.bot.netflix.db.model.User;
import com.google.gson.Gson;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import org.eclipse.microprofile.context.ManagedExecutor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@ApplicationScoped
public class UserRepository implements PanacheRepositoryBase<User, String> {

    @Inject
    ManagedExecutor managedExecutor;

    public Optional<User> findy(String user_id) {
        try {
            return CompletableFuture.supplyAsync(() -> findByIdOptional(user_id), managedExecutor).toCompletableFuture().get();

        }catch (Exception e){
            return Optional.empty();
        }
    }

    @Transactional
    public void remove(String user_id){
        Optional<User> user =findy(user_id);

        if(user.isPresent()){
            CompletableFuture.runAsync(user.get()::delete, managedExecutor);
        }
    }
}
