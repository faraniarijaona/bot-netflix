package com.bot.netflix.db.repository;

import com.bot.netflix.db.model.PaymentLog;
import com.bot.netflix.utilities.LogUtil;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.vertx.core.json.JsonObject;
import oma.logging.LogRecord;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.context.ManagedExecutor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@ApplicationScoped
public class PaymentLogRepository implements PanacheRepositoryBase<PaymentLog, String> {
    @Inject
    ManagedExecutor managedExecutor;

    @Inject
    LogUtil logUtil;

    private Logger LOGGER() {
        return logUtil.getLogger("dbLogger");
    }

    @Transactional
    public Optional<PaymentLog> findy(String pay_ref) {

        LogRecord LOG_RECORD = new LogRecord().setService("paymentlog.findy");
        JsonObject LOG_MESSAGE = new JsonObject().put("pay_ref", pay_ref);

        if(pay_ref!=null)
            pay_ref.trim();
        try {
            return CompletableFuture.supplyAsync(() ->{
               Optional<PaymentLog> optional = findByIdOptional(pay_ref);
                LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.isPaymentOK", optional.isPresent())));

                return  optional;
            }, managedExecutor).toCompletableFuture().get();

        }catch (Exception e){
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.isPaymentOK", false)));
            return Optional.empty();
        }
    }
}
