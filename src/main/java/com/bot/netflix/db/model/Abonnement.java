/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.netflix.db.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faraniarijaona
 */
@Entity
@Table(catalog = "netflix", schema = "", name = "abonnement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Abonnement.findAll", query = "SELECT a FROM Abonnement a"),
    @NamedQuery(name = "Abonnement.findById", query = "SELECT a FROM Abonnement a WHERE a.id = :id"),
    @NamedQuery(name = "Abonnement.findByCode", query = "SELECT a FROM Abonnement a WHERE a.code = :code"),
    @NamedQuery(name = "Abonnement.findByAmount", query = "SELECT a FROM Abonnement a WHERE a.amount = :amount"),
    @NamedQuery(name = "Abonnement.findByDuration", query = "SELECT a FROM Abonnement a WHERE a.duration = :duration"),
    @NamedQuery(name = "Abonnement.findByIsPaid", query = "SELECT a FROM Abonnement a WHERE a.isPaid = :isPaid"),
    @NamedQuery(name = "Abonnement.findByDate", query = "SELECT a FROM Abonnement a WHERE a.date = :date"),
    @NamedQuery(name = "Abonnement.findByDateEnd", query = "SELECT a FROM Abonnement a WHERE a.dateEnd = :dateEnd")})
public class Abonnement extends PanacheEntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private Double amount;
    private Integer duration;
    @Column(name = "is_paid")
    private Boolean isPaid;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "date_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEnd;
    @JoinColumn(name = "ref_netflix", referencedColumnName = "id")
    @ManyToOne
    private NetflixAccount netflixAccount;
    @JoinColumn(name = "ref_payment", referencedColumnName = "ref")
    @OneToOne
    private PaymentLog paymentLog;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne
    private User user;

    public Abonnement() {
    }

    public Abonnement(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public NetflixAccount getNetflixAccount() {
        return netflixAccount;
    }

    public void setNetflixAccount(NetflixAccount netflixAccount) {
        this.netflixAccount = netflixAccount;
    }

    public PaymentLog getPaymentLog() {
        return paymentLog;
    }

    public void setPaymentLog(PaymentLog paymentLog) {
        this.paymentLog = paymentLog;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Abonnement)) {
            return false;
        }
        Abonnement other = (Abonnement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "netflix.Abonnement[ id=" + id + " ]";
    }
    
}
