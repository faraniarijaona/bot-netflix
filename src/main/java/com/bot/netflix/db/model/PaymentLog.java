/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.netflix.db.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faraniarijaona
 */
@Entity
@Table(name = "payment_log", catalog = "netflix", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentLog.findAll", query = "SELECT p FROM PaymentLog p"),
    @NamedQuery(name = "PaymentLog.findByRef", query = "SELECT p FROM PaymentLog p WHERE p.ref = :ref"),
    @NamedQuery(name = "PaymentLog.findByAmount", query = "SELECT p FROM PaymentLog p WHERE p.amount = :amount"),
    @NamedQuery(name = "PaymentLog.findByDate", query = "SELECT p FROM PaymentLog p WHERE p.date = :date")})
public class PaymentLog extends PanacheEntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    private String ref;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private Double amount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @OneToOne(mappedBy = "paymentLog")
    private Abonnement abonnement;

    public PaymentLog() {
    }

    public PaymentLog(String ref) {
        this.ref = ref;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Abonnement getAbonnement() {
        return abonnement;
    }

    public void setAbonnement(Abonnement abonnement) {
        this.abonnement = abonnement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ref != null ? ref.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentLog)) {
            return false;
        }
        PaymentLog other = (PaymentLog) object;
        if ((this.ref == null && other.ref != null) || (this.ref != null && !this.ref.equals(other.ref))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "netflix.PaymentLog[ ref=" + ref + " ]";
    }
    
}
