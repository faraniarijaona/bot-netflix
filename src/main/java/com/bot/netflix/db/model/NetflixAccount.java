/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.netflix.db.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author faraniarijaona
 */
@Entity
@Table(name = "netflix_account", catalog = "netflix", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NetflixAccount.findAll", query = "SELECT n FROM NetflixAccount n"),
    @NamedQuery(name = "NetflixAccount.findById", query = "SELECT n FROM NetflixAccount n WHERE n.id = :id"),
    @NamedQuery(name = "NetflixAccount.findByRef", query = "SELECT n FROM NetflixAccount n WHERE n.ref = :ref"),
    @NamedQuery(name = "NetflixAccount.findByPassword", query = "SELECT n FROM NetflixAccount n WHERE n.password = :password"),
    @NamedQuery(name = "NetflixAccount.findByNumber", query = "SELECT n FROM NetflixAccount n WHERE n.number = :number"),
    @NamedQuery(name = "NetflixAccount.findByNextRenew", query = "SELECT n FROM NetflixAccount n WHERE n.nextRenew = :nextRenew")})
public class NetflixAccount extends PanacheEntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    private String ref;
    private String password;
    private Integer number;
    @Column(name = "next_renew")
    @Temporal(TemporalType.DATE)
    private Date nextRenew;
    @OneToMany(mappedBy = "netflixAccount")
    private Collection<Abonnement> abonnementCollection;

    public NetflixAccount() {
    }

    public NetflixAccount(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Date getNextRenew() {
        return nextRenew;
    }

    public void setNextRenew(Date nextRenew) {
        this.nextRenew = nextRenew;
    }

    @XmlTransient
    public Collection<Abonnement> getAbonnementCollection() {
        return abonnementCollection;
    }

    public void setAbonnementCollection(Collection<Abonnement> abonnementCollection) {
        this.abonnementCollection = abonnementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NetflixAccount)) {
            return false;
        }
        NetflixAccount other = (NetflixAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "netflix.NetflixAccount[ id=" + id + " ]";
    }
    
}
