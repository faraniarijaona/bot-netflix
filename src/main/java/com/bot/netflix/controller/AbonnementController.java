package com.bot.netflix.controller;

import com.bot.netflix.db.model.Abonnement;
import com.bot.netflix.db.model.NetflixAccount;
import com.bot.netflix.db.model.PaymentLog;
import com.bot.netflix.db.model.User;
import com.bot.netflix.db.repository.AbonnementRepository;
import com.bot.netflix.db.repository.PaymentLogRepository;
import com.bot.netflix.db.repository.UserRepository;
import com.bot.netflix.utilities.AbonnementUtilities;
import com.bot.netflix.utilities.LogUtil;
import com.bot.netflix.utilities.MessageUtilities;
import com.bot.netflix.utilities.parsingUtilities.ExtractReferenceUtil;
import com.google.gson.JsonObject;
import net.bytebuddy.utility.RandomString;
import oma.logging.LogRecord;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.config.ConfigProvider;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Path("/abonnement")
public class AbonnementController {
    @Inject
    LogUtil logUtil;

    private Logger LOGGER() {
        return logUtil.getLogger("abonnementLogger");
    }

    @Inject
    AbonnementRepository abonnementRepository;

    @Inject
    PaymentLogRepository paymentLogRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    AbonnementUtilities abonnementUtilities;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String abonner(
            @FormParam("duration") int duration,
            @FormParam("user_id") String userId,
            @FormParam("first_name") String firstName,
            @FormParam("last_name") String lastName,
            @FormParam("gender") String gender,
            @FormParam("profil_pic") String profilPic) {

        /*
         * Initiate Log Params
         */
        LogRecord LOG_RECORD = new LogRecord().setService("abonner");
        io.vertx.core.json.JsonObject LOG_MESSAGE = new io.vertx.core.json.JsonObject()
                .put("input.duration", duration)
                .put("input.user_id", userId)
                .put("input.firstName", firstName)
                .put("input.lastName", lastName)
                .put("input.gender", gender)
                .put("input.profilPic", profilPic);

        /*
         * Liste compte disponible
         */
        List<NetflixAccount> accounts = abonnementRepository.getListAvailableAccount();

        /*
         *tester si l'utilisateur est déjà un client (enregistré)
         */
        Optional<User> optionalUser = userRepository.findy(userId);

        /*
         * Voir si le client dispose un abonnement actif
         */
        Optional<Abonnement> abonnementActive = Optional.empty();
        if (optionalUser.isPresent()) {
            abonnementActive = optionalUser.get().getAbonnementCollection().stream()
                    .filter(abonnementUtilities::abonnementPredicate).max(abonnementUtilities::abonnementComparator);
        }

        /*
         * Si il n'y a pas compte dispo et que le client n'a pas abonnement en cours
         */
        if (accounts.size() == 0 && !abonnementActive.isPresent()) {
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "account_unavailable")));

            return MessageUtilities.createRedirectMessage("account_unavailable");
        }

        /*
         * Si l'utilisateur en question dispose un abonnement en attente de paiement
         */
        if (optionalUser.isPresent()) {
            Abonnement currentAbonnement = null;
            Optional<Abonnement> wait_for_payment = optionalUser.get().getAbonnementCollection().stream()
                    .filter(abonnement -> abonnement.getUser().getUserId().equals(userId) && !abonnement.getIsPaid())
                    .findFirst();

            if (wait_for_payment.isPresent()) {
                currentAbonnement = wait_for_payment.get();

                if ((currentAbonnement.getDate().getTime() + 7000000) < new Date().getTime()) {
                    abonnementRepository.supprimer(currentAbonnement.getCode(), currentAbonnement.getUser().getUserId());
                    currentAbonnement = null;
                } else {
                    abonnementUtilities.deleteAbonnement(currentAbonnement.getCode(),
                            currentAbonnement.getUser().getUserId(),
                            currentAbonnement.getDate().getTime() + 7200000 - new Date().getTime()
                    );
                }
            }

            if (currentAbonnement != null) {
                JsonObject attributes = new JsonObject();
                attributes.addProperty("amount", Long.valueOf(wait_for_payment.get().getAmount().longValue()).toString());
                attributes.addProperty("code", wait_for_payment.get().getCode());

                attributes.addProperty("duration", wait_for_payment.get().getDuration());

                Calendar c = Calendar.getInstance();
                c.setTime(wait_for_payment.get().getDate());
                c.add(Calendar.HOUR, 2);

                attributes.addProperty("payment_countdown", ChronoUnit.MINUTES.between(new Date().toInstant(),
                        c.getTime().toInstant()) + " minutes");
                attributes.addProperty("date_end", new SimpleDateFormat("dd-MM-yyyy").format(wait_for_payment.get().getDateEnd()));

                LOG_MESSAGE.put("response.message", "abonnement with pending payment");
                LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE));

                return MessageUtilities.createRedirectMessage(attributes, "payment");
            }
        }

        long amount = duration * ConfigProvider.getConfig().getValue("price.unit", Integer.class);

        /*
         * Voir si l'user en cours possède un abonnement qui est encore actif
         */
        Abonnement newAbonnement = new Abonnement();
        Calendar date_end = Calendar.getInstance();

        /*
         * S'il y a abonnement actif
         */
        if (abonnementActive.isPresent()) {
            date_end.setTime(abonnementActive.get().getDateEnd());
            newAbonnement.setNetflixAccount(abonnementActive.get().getNetflixAccount());
        }
        /*
         * S'il n'y a pas abonnement actif
         */
        else {
            newAbonnement.setNetflixAccount(accounts.get(0));
            int day_of_month = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            if (day_of_month > 15) {
                duration = duration + 1;

                if (day_of_month < 26) {
                    amount = amount + ConfigProvider.getConfig().getValue("price.surplus", Integer.class);
                }
            }
        }

        date_end.add(Calendar.MONTH, duration);
        date_end.set(Calendar.DAY_OF_MONTH, 9);
        date_end.set(Calendar.HOUR, 0);
        date_end.set(Calendar.MINUTE, 0);
        date_end.set(Calendar.SECOND, 0);

        User user = new User();
        user.setProfilPic(profilPic);
        user.setGender(gender);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setUserId(userId);
        user.setDate(Calendar.getInstance().getTime());


        String code = RandomString.make(ConfigProvider.getConfig().getValue("validation.code.length", Integer.class));
        newAbonnement.setAmount((double) amount);
        newAbonnement.setCode(code);
        newAbonnement.setDateEnd(date_end.getTime());
        newAbonnement.setDuration(duration);
        newAbonnement.setDate(new Date());
        newAbonnement.setIsPaid(false);
        newAbonnement.setUser(user);

        abonnementRepository.enregistrer(newAbonnement, optionalUser.isPresent());

        abonnementUtilities.deleteAbonnement(newAbonnement.getCode(), newAbonnement.getUser().getUserId());


        /*
         * Envoyer l'ecran demandnat le paiement
         */
        JsonObject attributes = new JsonObject();
        attributes.addProperty("amount", Long.valueOf(amount).toString());
        attributes.addProperty("code", code);
        attributes.addProperty("duration", duration);
        attributes.addProperty("payment_countdown", "2h");
        attributes.addProperty("date_end", new SimpleDateFormat("dd-MM-yyyy").format(newAbonnement.getDateEnd()));

        LOG_MESSAGE.put("response.message", "abonnement initiated successfuly");
        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE));

        return MessageUtilities.createRedirectMessage(attributes, "payment");
    }


    /**
     * Valider un paiment venant de l'utiisateur
     *
     * @param userId      identifiant de l'utilisateur
     * @param code        code de 06 caractères de la reservation de l'utilisateur
     * @param ref_payment reference de paiement à valider
     * @return
     */

    @POST
    @Path("/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public String validate(@FormParam("user_id") String userId,
                           @FormParam("code") String code,
                           @FormParam("ref_payment") String ref_payment) {

        /*
         * Initiate Log Params
         */
        LogRecord LOG_RECORD = new LogRecord().setService("validate");
        io.vertx.core.json.JsonObject LOG_MESSAGE = new io.vertx.core.json.JsonObject()
                .put("input.userId", userId)
                .put("input.code", code)
                .put("input.ref_payment", ref_payment);

        /*
         * Find payment references
         */
        List<String> mayBe = ExtractReferenceUtil.find(ref_payment);

        if (mayBe.size() == 0) {
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "payment_ref_incorrect")));
            return MessageUtilities.createRedirectMessage("payment_ref_incorrect");
        }

        Optional<PaymentLog> paymentLog = null;

        for (int i = 0; i < mayBe.size(); i++) {
            paymentLog = paymentLogRepository.findy(AbonnementUtilities.cleanRefPayment(mayBe.get(i)));
            if (paymentLog.isPresent())
                break;

            if (i == mayBe.size() - 1) {
                LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "payment_ref_incorrect")));
                return MessageUtilities.createRedirectMessage("payment_ref_incorrect");
            }
        }

        if (paymentLog.get().getAbonnement() != null) {
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "payment_ref_incorrect")));
            return MessageUtilities.createRedirectMessage("payment_ref_incorrect");
        }

        Optional<Abonnement> abonnement = abonnementRepository.rechercher(userId, code);

        if (!abonnement.isPresent()) {
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "abonnement_not_found")));
            return MessageUtilities.createRedirectMessage("abonnement_not_found");
        }

        if (paymentLog.get().getAmount().longValue() < abonnement.get().getAmount().longValue()) {
            JsonObject attributes = new JsonObject();
            attributes.addProperty("amount", abonnement.get().getAmount().longValue() + "Ar");
            attributes.addProperty("old_amount", paymentLog.get().getAmount().longValue() + "Ar");
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "payment_amount_insufficent")));
            return MessageUtilities.createRedirectMessage(attributes, "payment_amount_insufficent");
        }

        Abonnement abonnement1 = abonnement.get();
        abonnement1.setIsPaid(true);
        abonnement1.setPaymentLog(paymentLog.get());

        abonnementRepository.valideAbonnement(abonnement1);

        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "payment_success")));
        return MessageUtilities.createRedirectMessage("call_moncompte");
    }
}
