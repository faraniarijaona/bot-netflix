package com.bot.netflix.controller;

import com.bot.netflix.db.model.Abonnement;
import com.bot.netflix.db.model.User;
import com.bot.netflix.db.repository.UserRepository;
import com.bot.netflix.utilities.AbonnementUtilities;
import com.bot.netflix.utilities.LogUtil;
import com.bot.netflix.utilities.MessageUtilities;
import com.google.gson.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import oma.logging.LogRecord;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/user")
public class UserController {

    @Inject
    LogUtil logUtil;

    private Logger LOGGER() {
        return logUtil.getLogger("userLogger");
    }

    @Inject
    UserRepository userRepository;

    @Inject
    AbonnementUtilities abonnementUtilities;

    @GET
    @Path("/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String findUser(@PathParam("user_id") String user_id) {
        /*
         * Initiate Log Params
         */
        LogRecord LOG_RECORD = new LogRecord().setService("findUser");
        io.vertx.core.json.JsonObject LOG_MESSAGE = new io.vertx.core.json.JsonObject()
                .put("input.userId", user_id);

        Optional<User> user = userRepository.findByIdOptional(user_id);

        if (!user.isPresent()) {
            /*
             * si l'utilisateur n'existe pas
             */
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "new user")));
            return MessageUtilities.createRedirectMessage("menu_new_user");
        }

        if (user.get().getAbonnementCollection().size() == 0) {
            /*
             * Si l'utilisateur n'a fait aucun abonnement
             */
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "novice user")));
            MessageUtilities.createRedirectMessage("menu_novice_user");
        }

        boolean is_all_outdate = user.get().getAbonnementCollection()
                .stream()
                .filter(abonnement -> abonnement.getDateEnd().compareTo(new Date()) > 0)
                .collect(Collectors.toList()).size() == 0;

        if (is_all_outdate) {
            /*
             * Si l'utilisateur a deja fait un renouvellement auparavant
             */
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "old user")));
            return MessageUtilities.createRedirectMessage("menu_renew_user");
        }

        /*
         * Si l'utilisateur possede un abonnement en cours
         */
        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "active user")));
        return MessageUtilities.createRedirectMessage("menu_user");

    }

    /**
     * Donne au client les informations du copmpte : login, password, nom espace, date echeance
     *
     * @param user_id user_id du client
     * @return jsonapi , redirection vers block mon_compte
     */
    @GET
    @Path("/moncompte/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String moncompte(@PathParam("user_id") String user_id) {

        /*
         * Initiate Log Params
         */
        LogRecord LOG_RECORD = new LogRecord().setService("findUser");
        io.vertx.core.json.JsonObject LOG_MESSAGE = new io.vertx.core.json.JsonObject()
                .put("input.userId", user_id);

        Optional<User> user = userRepository.findByIdOptional(user_id);

        if (user.isPresent()) {

            Optional<Abonnement> abonnement = user.get().getAbonnementCollection().stream()
                    .filter(abonnementUtilities::abonnementPredicate)
                    .filter(abonnement1 -> abonnement1.getIsPaid())
                    .max(abonnementUtilities::abonnementComparator);

            if (abonnement.isPresent()) {
                Abonnement current = abonnement.get();

                JsonObject attributes = new JsonObject();
                attributes.addProperty("login", current.getNetflixAccount().getRef());
                attributes.addProperty("password", current.getNetflixAccount().getPassword());
                attributes.addProperty("espace", current.getNetflixAccount().getNumber());
                attributes.addProperty("date_end", new SimpleDateFormat("dd-MM-yyyy").format(current.getDateEnd()));

                LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.login", current.getNetflixAccount().getRef())
                        .put("response.espace", current.getNetflixAccount().getNumber())
                        .put("response.date_end", new SimpleDateFormat("dd-MM-yyyy").format(current.getDateEnd()))));

                return MessageUtilities.createRedirectMessage(attributes, "mon_compte");
            } else {
                /*
                 * user doit renouveller
                 */
                LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "new renewal user")));
                return MessageUtilities.createRedirectMessage("menu_renew_user");
            }

        } else {
            /*
             * User introuvable
             */
            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.message", "new user")));
            return MessageUtilities.createRedirectMessage("menu_new_user");
        }
    }
}
























/*    @POST
    public String addUser(@FormParam("user_id") String userId,
                          @FormParam("first_name") String firstName,
                          @FormParam("last_name") String lastName,
                          @FormParam("gender") String gender,
                          @FormParam("profil_pic") String profilPic){
        User newUser = new User()
                .setUserId(userId)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setGender(gender)
                .setProfilPic(profilPic);

        userRepository.save(newUser);

        Message message = new Message();
        List<String> blocks_name = new ArrayList<>();

        blocks_name.add("menu_novice_user");
        message.setmBlockNames(blocks_name);

        return CommonUtilities.toJson(message);
    }

    @GET
    @Path("/delete/{user_id}")
    public String removeUser(@PathParam("user_id") String userId){
        userRepository.remove(userId);

        Message message = new Message();
        List<String> blocks_name = new ArrayList<>();

        blocks_name.add("menu_novice_user");
        message.setmBlockNames(blocks_name);

        return CommonUtilities.toJson(message);
    }*/