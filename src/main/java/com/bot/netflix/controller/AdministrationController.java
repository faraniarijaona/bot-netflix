package com.bot.netflix.controller;

import com.bot.netflix.db.model.NetflixAccount;
import com.bot.netflix.db.model.PaymentLog;
import com.bot.netflix.fbelement.elements.Message;
import com.bot.netflix.utilities.AbonnementUtilities;
import com.bot.netflix.utilities.LogUtil;
import com.bot.netflix.utilities.MessageUtilities;
import com.bot.netflix.utilities.parsingUtilities.IParser;
import com.bot.netflix.utilities.parsingUtilities.ParserFactory;
import com.bot.netflix.utilities.response.Response;
import com.bot.netflix.utilities.response.Status;
import com.google.gson.Gson;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Parameters;
import io.vertx.core.json.JsonObject;
import javafx.util.Pair;
import oma.logging.LogRecord;
import org.apache.logging.log4j.Logger;
import sun.misc.MessageUtils;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;

@Path("/admin")
public class AdministrationController {

    @Inject
    LogUtil logUtil;

    private Logger LOGGER() {
        return logUtil.getLogger("adminLogger");
    }

    /**
     * Pour enregistrer un reference de paiement
     *
     * @param refpay ref du paiement
     * @param amount montant du paiement
     * @return reponse du traitement
     */
    @POST
    @Path("/payment/{ref_pay}/{amount}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String registerPayment(@PathParam("ref_pay") String refpay, @PathParam("amount") double amount) {
        LogRecord LOG_RECORD = new LogRecord().setService("registerPayment");
        JsonObject LOG_MESSAGE = new JsonObject().put("input.refpay", refpay).put("input.amount", amount);
        Response _response = new Response();

        refpay = AbonnementUtilities.cleanRefPayment(refpay);

        PaymentLog paymentLog = new PaymentLog();
        paymentLog.setRef(refpay);
        paymentLog.setAmount(amount);
        paymentLog.setDate(new Date());

        if (PaymentLog.findById(refpay) != null) {
            _response.setStatus(Status.NOK);
            _response.setMessage("Payment Code deja utilise");

            LOG_MESSAGE.put("response.status", _response.getStatus());
            LOG_MESSAGE.put("response.message", _response.getMessage());

            LOG_RECORD.setMessage(LOG_MESSAGE);
            LOGGER().info(LOG_RECORD);

            return MessageUtilities.toJson(_response);
        }

        paymentLog.persistAndFlush();

        _response.setStatus(Status.OK);
        _response.setMessage("Payment enregistre");

        LOG_MESSAGE.put("response.status", _response.getStatus());
        LOG_MESSAGE.put("response.message", _response.getMessage());
        LOG_RECORD.setMessage(LOG_MESSAGE);
        LOGGER().info(LOG_RECORD);

        return MessageUtilities.toJson(_response);
    }

    /**
     * Pour mettre à jour payment amount
     *
     * @param refpay ref
     * @param amount montant
     * @return reponse du traitement
     */
    @PUT
    @Path("/payment/{ref_pay}/{amount}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String editPayment(@PathParam("ref_pay") String refpay, @PathParam("amount") double amount) {
        LogRecord LOG_RECORD = new LogRecord().setService("editPayment");
        JsonObject LOG_MESSAGE = new JsonObject().put("input.refpay", refpay).put("input.amount", amount);
        Response _response = new Response();

        PaymentLog paymentLog = PaymentLog.findById(refpay);
        if (paymentLog == null) {

            _response.setStatus(Status.NOK);
            _response.setMessage("Payment code inexistant");

            LOG_MESSAGE.put("response.status", _response.getStatus());
            LOG_MESSAGE.put("response.message", _response.getMessage());

            LOG_RECORD.setMessage(LOG_MESSAGE);
            LOGGER().info(LOG_RECORD);

            return MessageUtilities.toJson(_response);
        }

        paymentLog.setAmount(amount);
        paymentLog.flush();

        _response.setStatus(Status.OK);
        _response.setMessage("modificiation enregistre");

        LOG_MESSAGE.put("response.status", _response.getStatus());
        LOG_MESSAGE.put("response.message", _response.getMessage());

        LOG_RECORD.setMessage(LOG_MESSAGE);
        LOGGER().info(LOG_RECORD);

        return MessageUtilities.toJson(_response);
    }

    /**
     * Pour supprimer un reference de paiement
     *
     * @param refpay ref
     * @return reponse du traitement
     */
    @DELETE
    @Path("/payment/{ref_pay}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String deletePayment(@PathParam("ref_pay") String refpay) {

        LogRecord LOG_RECORD = new LogRecord().setService("deletePayment");
        JsonObject LOG_MESSAGE = new JsonObject().put("input.refpay", refpay);
        Response _response = new Response();

        PaymentLog paymentLog = PaymentLog.findById(refpay);
        if (paymentLog == null) {
            _response.setStatus(Status.NOK);
            _response.setMessage("Payment code inexistant");

            LOG_MESSAGE.put("response.status", _response.getStatus());
            LOG_MESSAGE.put("response.message", _response.getMessage());

            LOG_RECORD.setMessage(LOG_MESSAGE);
            LOGGER().info(LOG_RECORD);

            return MessageUtilities.toJson(_response);
        }

        paymentLog.delete();

        _response.setStatus(Status.OK);
        _response.setMessage("suppression réussie");

        LOG_MESSAGE.put("response.status", _response.getStatus());
        LOG_MESSAGE.put("response.message", _response.getMessage());

        LOG_RECORD.setMessage(LOG_MESSAGE);
        LOGGER().info(LOG_RECORD);

        return MessageUtilities.toJson(_response);
    }

    /**
     * ajouetr un compte netflix
     *
     * @param ref
     * @param pass
     * @return
     */
    @POST
    @Path("/netflix/{ref}/{pass}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String registerNetflixAccount(@PathParam("ref") String ref, @PathParam("pass") String pass) {
        LogRecord LOG_RECORD = new LogRecord().setService("registerNetflixAccount");
        JsonObject LOG_MESSAGE = new JsonObject().put("input.ref", ref);
        Response _response = new Response();

        if (NetflixAccount.list("ref = :ref", Parameters.with("ref", ref)).size() > 0) {
            _response.setStatus(Status.NOK);
            _response.setMessage("NetflixAccount deja utilise");

            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

            return MessageUtilities.toJson(_response);
        }

        for (int i = 1; i <= 5; i++) {
            NetflixAccount netflixAccount = new NetflixAccount();
            netflixAccount.setNumber(i);
            netflixAccount.setRef(ref);
            netflixAccount.setPassword(pass);
            netflixAccount.persistAndFlush();
        }

        _response.setStatus(Status.OK);
        _response.setMessage("compte enregistre");

        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

        return MessageUtilities.toJson(_response);
    }

    @PUT
    @Path("/netflix/{ref}/{pass}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String editNetflixAccount(@PathParam("ref") String ref, @PathParam("pass") String pass) {
        LogRecord LOG_RECORD = new LogRecord().setService("editNetflixAccount");
        JsonObject LOG_MESSAGE = new JsonObject().put("input.ref", ref);
        Response _response = new Response();

        List<NetflixAccount> netflixAccountList = NetflixAccount.list("ref = :ref", Parameters.with("ref", ref));
        if (netflixAccountList.size() == 0) {
            _response.setStatus(Status.NOK);
            _response.setMessage("compte pas encore enregistre");

            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

            return MessageUtilities.toJson(_response);
        }

        netflixAccountList.forEach(netflixAccount -> {
            netflixAccount.setPassword(pass);
            netflixAccount.flush();
        });

        _response.setStatus(Status.OK);
        _response.setMessage("modification enregistree");

        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

        return MessageUtilities.toJson(_response);
    }

    @PUT
    @Path("/netflix/putall/{pass}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String putAllNetflixAccount(@PathParam("pass") String pass) {
        LogRecord LOG_RECORD = new LogRecord().setService("putAllNetflixAccount");
        JsonObject LOG_MESSAGE = new JsonObject();
        Response _response = new Response();

        List<NetflixAccount> netflixAccountList = NetflixAccount.listAll();

        netflixAccountList.forEach(netflixAccount -> {
            netflixAccount.setPassword(pass);
            netflixAccount.flush();
        });

        _response.setStatus(Status.OK);
        _response.setMessage("modification enregistree");

        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

        return MessageUtilities.toJson(_response);
    }

    @DELETE
    @Path("/netflix/{ref}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteNetflixAccount(@PathParam("ref") String ref) {
        LogRecord LOG_RECORD = new LogRecord().setService("deleteNetflixAccount");
        JsonObject LOG_MESSAGE = new JsonObject().put("input.ref", ref);
        Response _response = new Response();

        List<NetflixAccount> netflixAccountList = NetflixAccount.list("ref = :ref", Parameters.with("ref", ref));
        if (netflixAccountList.size() == 0) {
            _response.setStatus(Status.NOK);
            _response.setMessage("compte inexistant");

            LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

            return MessageUtilities.toJson(_response);
        }

        netflixAccountList.forEach(netflixAccount -> {
            if (netflixAccount.getAbonnementCollection() != null) {
                netflixAccount.getAbonnementCollection().forEach(PanacheEntityBase::delete);
            }
            netflixAccount.delete();
            netflixAccount.flush();
        });


        _response.setStatus(Status.OK);
        _response.setMessage("suppression reussie");

        LOGGER().info(LOG_RECORD.setMessage(LOG_MESSAGE.put("response.status", _response.getStatus()).put("response.message", _response.getMessage())));

        return MessageUtilities.toJson(_response);
    }

    /**
     * recevoir SMS de paiement dens l'objectif de le traiter
     *
     * @param sender  numero du sender
     * @param message le corps du message
     * @return retour de l'action
     */
    @POST
    @Path("/sms")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public String parseSMS(@FormParam("sender") String sender, @FormParam("message") String message) {
        LogRecord record = new LogRecord("parseSMS");
        JsonObject messazy = new JsonObject().put("sender", sender).put("message", message);

        Response response = new Response();
        response.setStatus(Status.NOK);

        IParser iParser = ParserFactory.getInstance(sender);

        if (iParser == null) {
            response.setMessage("unknown sender");
            return MessageUtilities.toJson(response);
        }

        Pair<String, Double> result = iParser.parse(message);

        if (result == null) {
            response.setMessage("message not contains payment");
            LOGGER().info(record.setMessage(messazy.put("error.message","message not contains payment")));
            return MessageUtilities.toJson(response);
        }

        if(PaymentLog.findById(result.getKey()) == null) {
            PaymentLog paymentLog = new PaymentLog();
            paymentLog.setAmount(result.getValue());
            paymentLog.setRef(result.getKey());
            paymentLog.setDate(new Date());

            paymentLog.persistAndFlush();
            response.setMessage("success");
        }
        else {
            response.setMessage("ref already registered");
        }

        response.setStatus(Status.OK);

        LOGGER().info(record.setMessage(messazy.put("success","ref:"+result.getKey()+", amount:"+result.getValue())));
        return MessageUtilities.toJson(response);
    }
}
