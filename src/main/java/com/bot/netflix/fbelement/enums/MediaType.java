package com.bot.netflix.fbelement.enums;

public enum MediaType {
    image,
    video
}
