package com.bot.netflix.fbelement.enums;

public enum TemplateType {
    generic,
    receipt,
    button,
    media,
    list
}
