package com.bot.netflix.fbelement.enums;

public enum AttachementType {
    template,
    image,
    audio,
    file,
    video
}
