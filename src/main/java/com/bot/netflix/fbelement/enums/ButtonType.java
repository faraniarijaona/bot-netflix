package com.bot.netflix.fbelement.enums;

public enum ButtonType {
    web_url,
    show_block,
    json_plugin_url,
    phone_number,
    postback
}
