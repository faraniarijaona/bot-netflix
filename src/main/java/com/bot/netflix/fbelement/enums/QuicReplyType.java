package com.bot.netflix.fbelement.enums;

public enum  QuicReplyType {
    text,
    user_phone_number,
    user_email
}
