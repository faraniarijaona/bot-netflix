
package com.bot.netflix.fbelement.elements;

import com.bot.netflix.fbelement.enums.QuicReplyType;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class QuickReply {

    @SerializedName("block_names")
    private List<String> mBlockNames;
    @SerializedName("set_attributes")
    private SetAttributes mSetAttributes;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("type")
    private String mType;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("content_type")
    private QuicReplyType contentType;

    public List<String> getBlockNames() {
        return mBlockNames;
    }

    public void setBlockNames(List<String> blockNames) {
        mBlockNames = blockNames;
    }

    public SetAttributes getSetAttributes() {
        return mSetAttributes;
    }

    public void setSetAttributes(SetAttributes setAttributes) {
        mSetAttributes = setAttributes;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
