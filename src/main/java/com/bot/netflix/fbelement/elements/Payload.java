
package com.bot.netflix.fbelement.elements;

import com.bot.netflix.fbelement.enums.Currency;
import com.bot.netflix.fbelement.enums.TemplateType;
import com.bot.netflix.fbelement.enums.TopElementStyle;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Payload {
    @SerializedName("address")
    private Address mAddress;
    @SerializedName("adjustments")
    private List<Adjustment> mAdjustments;
    @SerializedName("currency")
    private Currency mCurrency;
    @SerializedName("elements")
    private List<Element> mElements;
    @SerializedName("order_number")
    private String mOrderNumber;
    @SerializedName("order_url")
    private String mOrderUrl;
    @SerializedName("payment_method")
    private String mPaymentMethod;
    @SerializedName("recipient_name")
    private String mRecipientName;
    @SerializedName("summary")
    private Summary mSummary;
    @SerializedName("template_type")
    private TemplateType mTemplateType;
    @SerializedName("top_element_style")
    private TopElementStyle topElementStyle;
    @SerializedName("timestamp")
    private String mTimestamp;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("image_aspect_ratio")
    private String imageAspectRatio;
    @SerializedName("buttons")
    private List<Button> mButtons;

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address address) {
        mAddress = address;
    }

    public List<Adjustment>  getAdjustments() {
        return mAdjustments;
    }

    public void setAdjustments(List<Adjustment>  adjustments) {
        mAdjustments = adjustments;
    }

    public Currency getCurrency() {
        return mCurrency;
    }

    public void setCurrency(Currency currency) {
        mCurrency = currency;
    }

    public List<Element> getElements() {
        return mElements;
    }

    public void setElements(List<Element> elements) {
        mElements = elements;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        mOrderNumber = orderNumber;
    }

    public String getOrderUrl() {
        return mOrderUrl;
    }

    public void setOrderUrl(String orderUrl) {
        mOrderUrl = orderUrl;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public String getRecipientName() {
        return mRecipientName;
    }

    public void setRecipientName(String recipientName) {
        mRecipientName = recipientName;
    }

    public Summary getSummary() {
        return mSummary;
    }

    public void setSummary(Summary summary) {
        mSummary = summary;
    }

    public TemplateType getTemplateType() {
        return mTemplateType;
    }

    public void setTemplateType(TemplateType templateType) {
        mTemplateType = templateType;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getImageAspectRatio() {
        return imageAspectRatio;
    }

    public void setImageAspectRatio(String imageAspectRatio) {
        this.imageAspectRatio = imageAspectRatio;
    }

    public TopElementStyle getTopElementStyle() {
        return topElementStyle;
    }

    public void setTopElementStyle(TopElementStyle topElementStyle) {
        this.topElementStyle = topElementStyle;
    }

    public List<Button> getmButtons() {
        return mButtons;
    }

    public void setmButtons(List<Button> mButtons) {
        this.mButtons = mButtons;
    }
}
