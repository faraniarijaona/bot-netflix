
package com.bot.netflix.fbelement.elements;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Message {

    @SerializedName("set_attributes")
    private JsonObject mSetAttributes;

    @SerializedName("messages")
    private List<Content> mMessage;

    @SerializedName("redirect_to_blocks")
    private List<String> mBlockNames;

    public List<Content> getMessage() {
        return mMessage;
    }

    public void setMessage(List<Content> message) {
        mMessage = message;
    }

    public List<String> getmBlockNames() {
        return mBlockNames;
    }

    public void setmBlockNames(List<String> mBlockNames) {
        this.mBlockNames = mBlockNames;
    }

    public JsonObject getmSetAttributes() {
        return mSetAttributes;
    }

    public void setmSetAttributes(JsonObject mSetAttributes) {
        this.mSetAttributes = mSetAttributes;
    }
}
