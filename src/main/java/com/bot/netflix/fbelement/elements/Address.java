
package com.bot.netflix.fbelement.elements;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Address {

    @SerializedName("city")
    private String mCity;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("postal_code")
    private String mPostalCode;
    @SerializedName("state")
    private String mState;
    @SerializedName("street_1")
    private String mStreet1;
    @SerializedName("street_2")
    private String mStreet2;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getPostalCode() {
        return mPostalCode;
    }

    public void setPostalCode(String postalCode) {
        mPostalCode = postalCode;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStreet1() {
        return mStreet1;
    }

    public void setStreet1(String street1) {
        mStreet1 = street1;
    }

    public String getStreet2() {
        return mStreet2;
    }

    public void setStreet2(String street2) {
        mStreet2 = street2;
    }

}
