
package com.bot.netflix.fbelement.elements;

import com.bot.netflix.fbelement.enums.Currency;
import com.bot.netflix.fbelement.enums.MediaType;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Element {

    @SerializedName("buttons")
    private List<Button> mButtons;
    @SerializedName("default_action")
    private Button defaultAction;
    @SerializedName("currency")
    private Currency mCurrency;
    @SerializedName("image_url")
    private String mImageUrl;
    @SerializedName("media_type")
    private MediaType mMediaType;
    @SerializedName("price")
    private Long mPrice;
    @SerializedName("quantity")
    private Long mQuantity;
    @SerializedName("subtitle")
    private String mSubtitle;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("url")
    private String mUrl;

    public List<Button> getButtons() {
        return mButtons;
    }

    public void setButtons(List<Button> buttons) {
        mButtons = buttons;
    }

    public Currency getCurrency() {
        return mCurrency;
    }

    public void setCurrency(Currency currency) {
        mCurrency = currency;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public MediaType getMediaType() {
        return mMediaType;
    }

    public void setMediaType(MediaType mediaType) {
        mMediaType = mediaType;
    }

    public Long getPrice() {
        return mPrice;
    }

    public void setPrice(Long price) {
        mPrice = price;
    }

    public Long getQuantity() {
        return mQuantity;
    }

    public void setQuantity(Long quantity) {
        mQuantity = quantity;
    }

    public String getSubtitle() {
        return mSubtitle;
    }

    public void setSubtitle(String subtitle) {
        mSubtitle = subtitle;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public Button getDefaultAction() {
        return defaultAction;
    }

    public void setDefaultAction(Button defaultAction) {
        this.defaultAction = defaultAction;
    }
}
