
package com.bot.netflix.fbelement.elements;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SetAttributes {

    @SerializedName("feedback")
    private String mFeedback;
    @SerializedName("number")
    private String mNumber;

    public String getFeedback() {
        return mFeedback;
    }

    public void setFeedback(String feedback) {
        mFeedback = feedback;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

}
