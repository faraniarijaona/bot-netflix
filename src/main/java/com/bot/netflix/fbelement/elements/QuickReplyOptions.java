
package com.bot.netflix.fbelement.elements;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class QuickReplyOptions {

    @SerializedName("process_text_by_ai")
    private Boolean mProcessTextByAi;
    @SerializedName("text_attribute_name")
    private String mTextAttributeName;

    public Boolean getProcessTextByAi() {
        return mProcessTextByAi;
    }

    public void setProcessTextByAi(Boolean processTextByAi) {
        mProcessTextByAi = processTextByAi;
    }

    public String getTextAttributeName() {
        return mTextAttributeName;
    }

    public void setTextAttributeName(String textAttributeName) {
        mTextAttributeName = textAttributeName;
    }
}
