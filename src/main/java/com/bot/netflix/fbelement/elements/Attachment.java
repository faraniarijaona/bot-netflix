
package com.bot.netflix.fbelement.elements;

import com.bot.netflix.fbelement.enums.AttachementType;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Attachment {

    @SerializedName("payload")
    private Payload mPayload;
    @SerializedName("type")
    private AttachementType mType;

    public Payload getPayload() {
        return mPayload;
    }

    public void setPayload(Payload payload) {
        mPayload = payload;
    }

    public AttachementType getType() {
        return mType;
    }

    public void setType(AttachementType type) {
        mType = type;
    }

}
