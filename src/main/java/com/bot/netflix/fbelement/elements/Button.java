
package com.bot.netflix.fbelement.elements;

import com.bot.netflix.fbelement.enums.ButtonType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Button {

    @SerializedName("block_names")
    private List<String> blockNames;
    @SerializedName("phone_number")
    private String phoneNumber;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("type")
    private ButtonType buttonType;
    @Expose
    @SerializedName("url")
    private String url;
    @Expose
    @SerializedName("messenger_extensions")
    private boolean messengerExtensions;
    @Expose
    @SerializedName("payload")
    private Payload payload;
    @Expose
    @SerializedName("set_attributes")
    private AttributeChatfuel setAttributes;

    public List<String> getBlockNames() {
        return blockNames;
    }

    public void setBlockNames(List<String> blockNames) {
        this.blockNames = blockNames;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ButtonType getButtonType() {
        return buttonType;
    }

    public void setButtonType(ButtonType buttonType) {
        this.buttonType = buttonType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isMessengerExtensions() {
        return messengerExtensions;
    }

    public void setMessengerExtensions(boolean messengerExtensions) {
        this.messengerExtensions = messengerExtensions;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public AttributeChatfuel getSetAttributes() {
        return setAttributes;
    }

    public void setSetAttributes(AttributeChatfuel setAttributes) {
        this.setAttributes = setAttributes;
    }
}
