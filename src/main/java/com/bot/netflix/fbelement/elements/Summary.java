
package com.bot.netflix.fbelement.elements;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Summary {

    @SerializedName("shipping_cost")
    private Double mShippingCost;
    @SerializedName("subtotal")
    private Long mSubtotal;
    @SerializedName("total_cost")
    private Double mTotalCost;
    @SerializedName("total_tax")
    private Long mTotalTax;

    public Double getShippingCost() {
        return mShippingCost;
    }

    public void setShippingCost(Double shippingCost) {
        mShippingCost = shippingCost;
    }

    public Long getSubtotal() {
        return mSubtotal;
    }

    public void setSubtotal(Long subtotal) {
        mSubtotal = subtotal;
    }

    public Double getTotalCost() {
        return mTotalCost;
    }

    public void setTotalCost(Double totalCost) {
        mTotalCost = totalCost;
    }

    public Long getTotalTax() {
        return mTotalTax;
    }

    public void setTotalTax(Long totalTax) {
        mTotalTax = totalTax;
    }

}
