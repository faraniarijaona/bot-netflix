
package com.bot.netflix.fbelement.elements;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Content {

    @SerializedName("attachment")
    private Attachment mAttachment;
    @SerializedName("quick_replies")
    private List<QuickReply> mQuickReplies;
    @SerializedName("quick_reply_options")
    private QuickReplyOptions mQuickReplyOptions;
    @SerializedName("text")
    private String mText;
    @SerializedName("redirect_to_blocks")
    private List<String> redirectToBlocks;
    @SerializedName("set_attributes")
    private JsonObject setAttributes;

    public Attachment getAttachment() {
        return mAttachment;
    }

    public void setAttachment(Attachment attachment) {
        mAttachment = attachment;
    }

    public List<QuickReply> getQuickReplies() {
        return mQuickReplies;
    }

    public void setQuickReplies(List<QuickReply> quickReplies) {
        mQuickReplies = quickReplies;
    }

    public QuickReplyOptions getQuickReplyOptions() {
        return mQuickReplyOptions;
    }

    public void setQuickReplyOptions(QuickReplyOptions quickReplyOptions) {
        mQuickReplyOptions = quickReplyOptions;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public List<String> getRedirectToBlocks() {
        return redirectToBlocks;
    }

    public void setRedirectToBlocks(List<String> redirectToBlocks) {
        this.redirectToBlocks = redirectToBlocks;
    }

    public JsonObject getSetAttributes() {
        return setAttributes;
    }

    public void setSetAttributes(JsonObject setAttributes) {
        this.setAttributes = setAttributes;
    }
}
